#include "maintenance.hpp"

using namespace std;


//BIKE
//getters
vector<Part> Bike::getParts() {
	return this->partsList;
}

string Bike::getName() {
	return this->name;
}

string Bike::getDesc() {
	return this->description;
}

int Bike::getHour() {
	return this->hour;
}

//setters
bool Bike::addPart(string name_, int itvl_) {
	Part n = Part(name_);
	n.setItvl(itvl_);
	n.setHour(0);
	this->partsList.push_back(n);
	this->numParts++;
	return true;
}

bool Bike::setName(string name_) {
	this->name = name_;
}

bool Bike::setDesc(string desc_) {
	this->description = desc_;
}

bool Bike::setHour(int i) {
	this->hour = i;
}

//funtions

int Bike::searchParts(string name_) {
	int i=0;
	while(i <= numParts) {
		if (this->partsList[i].getName() == name_) {
			return i;
		}
		i++;
	}
	return -1;
}

bool Bike::maintainPart(string name_) {
	int i = this->searchParts(name_);
	if (i >= 0) {
		partsList[i].setHour(getHour());
		return true;
	}
	return false;
}

void Bike::checkParts() {
	int i=0;
	while(i <= numParts) {
		string n = partsList[i].getName();
		if (partsList[i].checkPart(this->getHour()) == true) {
			int t = this->getHour() - (partsList[i].getHour() + partsList[i].getItvl());
			cout << n << " due for maintenance, " << t << " hours overdue.\n";
		}
		else {
			int t = (partsList[i].getHour() + partsList[i].getItvl()) - this->getHour();
			cout << n << " due for maintenance in " << t << " hours.\n";
		}
		i++;
	}
}

void Bike::printLog() {
	printf("This function hasn't been implemented yet.");
}


//PART
//getters
int Part::getHour() {
	return this->hour;
}

int Part::getItvl() {
	return this->itvl;
}

string Part::getName() {
	return this->name;
}

string Part::getDesc() {
	return this->description;
}

bool Part::setHour(int i) {
	this->hour = i;
	return true;
}

bool Part::setItvl(int i) {
	this->itvl = i;
	return true;
}

bool Part::setName(string name_) {
	this->name = name_;
	return true;
}

bool Part::setDesc(string desc_) {
	this->description = desc_;
	return true;
}

//functions

bool Part::checkPart(int curr) {
	if (curr >= (this->hour + this->itvl)) {
		return true;
	}
	return false;
}