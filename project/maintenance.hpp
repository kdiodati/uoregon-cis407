#include <string>
#include <vector>
#include <iostream>
#include <stdio.h>

using namespace std;

class Part {
	public:
		//constructors
		Part(string name_) {
			name = name_;
		}
		~Part();

		//getters
		int getHour();
		int getItvl();
		string getName();
		string getDesc();

		//setters
		bool setHour(int i);
		bool setItvl(int i);
		bool setName(string name_);
		bool setDesc(string desc_);

		//functions
		bool checkPart(int curr);
	private:
		int itvl;
		int hour;
		string name;
		string description;
};

class Bike {
	public:
		//constructors
		Bike(string name_) {
			name = name_;
			numParts = 0;
		}
		~Bike();

		//getters
		vector<Part> getParts();
		string getName();
		string getDesc();
		int getHour();

		//setters
		bool addPart(string name_, int itvl_);
		bool setName(string name_);
		bool setDesc(string desc_);
		bool setHour(int i);

		//functions
		int searchParts(string name_);
		bool maintainPart(string name_);
		void checkParts();
		void printLog();
	private:
		vector<Part> partsList;
		string name;
		string description;
		int hour;
		int numParts;
		vector<string> log;
};
