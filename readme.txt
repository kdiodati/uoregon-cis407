						    =========================
						    DirtBike Maintence Logger
						    =========================

Unlike the cars and motorcycles that drive on the roads and highways all across the world, where maintence is done by the mile.
Dirtbike maintence is done by the hour the engine has been running. And although maintence is rather simple it can be hard to
keep track of when parts were maintained and when they should be fixed/replaced next as there are quite a few parts to these
machines. My goal is to create an application where you can create a bike and all the parts on it, then be able to keep track
of maintence and add alerts to whenever a part should be fixed/replaced if the time is near or passed. Most riders add an hour
meter to their motorcycles so that they can keep track of how long it has been running, so the application will just let you add
the new time that is on the bike and it will calculate everything for you. As well as you can input when a part is fixed/replaced
and it will set that part as fixed.




--------
WORKFLOW
--------
*Get basic concept working through terminal
*Add features (saving/loading,step-by-step new bike builder, etc...)
*Make GUI
*Add extra functionality



--------------
FILE STRUCTURE
--------------
Garage/               -A folder where all bikes will be stored
maintence.cpp/.hpp    -Contains the bike and part classes, where most of the computing will take place
menu.cpp/.hpp         -Runs the main-menu and ability to add/remove bikes, open saved bikes, etc.
bikeMenu.cpp/.hpp     -Runs the bike-menu, where you can view maintence items and add to the log
(more to be added as needed)



